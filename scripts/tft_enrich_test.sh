#!/bin/sh
homedir='/home/asaha6/github/tftarget'
outdir='results/'
cd $homedir

# create output direcctory structure
mkdir $outdir

# call enrichment function
gold=$homedir/data/tf-target-chea.txt
net=$homedir/data/test/net.mtx
label=$homedir/data/test/nodes.txt
tf=$homedir/data/test/tf_list.txt
target=$homedir/data/test/target_list.txt
n=10000
process=10
d=2
model=weibull
bg=$outdir/bg_data.txt
plot=$outdir/bg_plot.png
common=$outdir/common_gold.txt

# call with full parameter list
python src/test_tft_enrichment.py -g $gold -net $net -l $label -tf $tf -target $target -n $n -p $process -d $d -m $model -bg $bg -plot $plot -c $common

# call with minimum number of parameters
#python src/test_tft_enrichment.py -net $net -l $label
