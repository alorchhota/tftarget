This is a tool to calculate tf-target enrichment of a network, given the golden network.

### How to run ?###
You have to provide the adjacency matrix (file format: [Matrix Market](http://math.nist.gov/MatrixMarket/formats.html)), and node labels (file format: one label per line) of your network as input, and you would get a p-value of your network.

This tool has ben deployed in HHPC along with sample data (location: /scratch1/battle-fs1/ashis/programs/tftarget). Please find bash scripts below to run this tool from command line. Warning: HHPC may not contain the latest version!


```
#!shell script

#!/bin/sh
homedir='/scratch1/battle-fs1/ashis/programs/tftarget'
outdir='results/'
cd $homedir

# define parameters
gold=$homedir/data/tf-target-chea.txt
net=$homedir/data/test/net.mtx
label=$homedir/data/test/nodes.txt
tf=$homedir/data/test/tf_list.txt
target=$homedir/data/test/target_list.txt
n=10000
process=10
d=2
model=weibull
bg=$outdir/bg_data.txt
plot=$outdir/bg_plot.png
common=$outdir/common_gold.txt

# call with full parameter list
python src/test_tft_enrichment.py -g $gold -net $net -l $label -tf $tf -target $target -n $n -p $process -d $d -m $model -bg $bg -plot $plot -c $common

# call with minimum number of parameters
#python src/test_tft_enrichment.py -net $net -l $label
```

### Script Options ###


```
#!help

'-g', '--gold': path to golden tf-target data file.
'-net', '--network': path to a file containing adjacency matrix of the network to test. the file must be in Matrix Market format.
'-l', '--label': path to a file containing node labels. one label in each line. Label ordering must be compatible with the network adjacency matrix.
'-tf': path to a file containing full TF list. one transcription factor in each line. order is not important. If None, all nodes used.
'-target': path to a file containing full target list. one target in each line. order is not important. If None, all nodes used.
'-n': number of random networks to construct background distribution.
'-p', '--process': number of processes. it is used to speed up execution.
'-directed': if this option is used, the network is considered as a directed graph. otherwise undirected.
'-d', '--distance': max distance between tf and target in network.
'-m', '--model': model distribution to fit background data of fraction of edges in the gold dataset. options: 'none', 'norm','beta', 'weibull'.
'-plot': output path to save background distribution plot.
'-bg', '--background': output path to save bacground distribution data.
'-c', '--common': output path to save edges common with gold network.
```


### Contact ###

* ashis@jhu.edu