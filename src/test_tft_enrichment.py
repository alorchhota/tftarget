'''
This script serves as an interface to calculate tf-target enrichment with user-given inputs.
'''

import os
import sys
sys.path.append(os.path.dirname(__file__))
import pandas as pd
import tf_target_enrichment as tte
import time
import argparse
import multiprocessing as mp
import scipy.io as sio

print(os.path.basename(__file__) +  ': parsing arguments ...')
parser = argparse.ArgumentParser()
parser.add_argument('-g', '--gold',
                    help='path to golden tf-target data file.',
                    default='data/tf-target-chea.txt')
parser.add_argument('-net', '--network',
                    help='path to a file containing adjacency matrix of the network to test. the file must be in Matrix Market format.')
parser.add_argument('-l', '--label',
                    help='path to a file containing node labels. one label in each line. Label ordering must be compatible with the network adjacency matrix.')
parser.add_argument('-tf',
                    help='path to a file containing full TF list. one transcription factor in each line. order is not important. If None, all nodes used.',
                    default=None)
parser.add_argument('-target',
                    help='path to a file containing full target list. one target in each line. order is not important. If None, all nodes used.',
                    default=None)
parser.add_argument('-n',
                    help='number of random networks to construct background distribution',
                    type=int,
                    default=100)
parser.add_argument('-p', '--process',
                    help='number of processes. it is used to speed up execution.',
                    type=int,
                    default=mp.cpu_count())
parser.add_argument('-directed',
                    help='is the network directed?',
                    action="store_true")
parser.add_argument('-d', '--distance',
                    help='max distance between tf and target in network.',
                    type=int,
                    default=1)
parser.add_argument('-m', '--model',
                    help='model distribution to fit number/fraction of edges in the gold dataset.',
                    choices=['none', 'norm','beta', 'weibull'],
                    default='none')
parser.add_argument('-plot',
                    help='output path to save background distribution plot.',
                    default=None)
parser.add_argument('-bg', '--background',
                    help='output path to save bacground distribution data.',
                    default=None)
parser.add_argument('-c', '--common',
                    help='output path to save edges common with gold network.',
                    default=None)

args = parser.parse_args()

''' setting variables '''
gold_tft_fn = args.gold
adj_fn = args.network
node_list_fn = args.label
tf_list_fn = args.tf if args.tf is not None else node_list_fn
target_list_fn = args.target if args.target is not None else node_list_fn
plot_dest_fn = args.plot
bg_data_dest_fn = args.background
n_trial = args.n
n_proc = args.process
d = args.distance
directed = args.directed
model_dist = args.model
common_edge_dest_fn = args.common

print('reading inputs ...')
# read adjacency matrix
A = sio.mmread(adj_fn)

# read node list
node_list_data = pd.read_table(node_list_fn, sep='\t', header=None, index_col=None)
nodes = node_list_data[0].values.tolist()

tf_list_data = pd.read_table(tf_list_fn, sep='\t', header=None, index_col=None)
tfs = tf_list_data[0].values.tolist()

target_list_data = pd.read_table(target_list_fn, sep='\t', header=None, index_col=None)
targets = target_list_data[0].values.tolist()

# sanity check
if A.shape[0] != A.shape[1]:
    raise 'adjacency matrix must be a square matrix'
if A.shape[0] != len(nodes):
    raise 'dimension of adjacency matrix must match with the number of node labels.'


# load gold tf-targets
tte.load_gold_tft_from_file(gold_tft_fn, header=0, index_col=None, tfcol='tf', targetcol='target')

print('calculating enrichment ...')
# check enrichment of the given network
t1 = time.time()
enr = tte.enrichment(A,
                     nodes,
                     tfs=tfs,
                     targets=targets,
                     n_trial=n_trial,
                     n_proc=n_proc,
                     bg_plot_dest=plot_dest_fn,
                     bg_data_dest=bg_data_dest_fn,
                     model_dist=model_dist,
                     d=d,
                     directed=directed)
t2 = time.time()

if common_edge_dest_fn is not None:
    with open(common_edge_dest_fn, 'w') as fh:
        text = '\n'.join([ '\t'.join([str(item) for item in e]) for e in enr[0]])
        fh.write(text)

print('#gold tf-targets in given network: ' + str(len(enr[0])) + '\np-value: ' + str(enr[1]) + '\nempirical p-value: ' + str(enr[2]))
#print('enrichment calculation duration: ' + str(t2-t1) + ' sec')
