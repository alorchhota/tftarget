'''
This script creates inputs for tf-target enrichment calculations from significant TF-Gene pairs (output of asenet).
'''
import os
import sys
sys.path.append(os.path.dirname(__file__))
import pandas as pd
import numpy as np
import argparse
import scipy.sparse as sp
import scipy.io as sio

print(os.path.basename(__file__) +  ': parsing arguments ...')
parser = argparse.ArgumentParser()
parser.add_argument('-tfdata',
                    help='path to tf list data file.',
                    default='data/test/tf_list.txt')
parser.add_argument('-targetdata',
                    help='path to target data file.',
                    default='data/test/target_list.txt')
parser.add_argument('-sigtft',
                    help='path to significant tf-target pairs.',
                    default='data/test/significant_tf_ase_fdr_bh.txt')
parser.add_argument('-netdest',
                    help='output path to save adjacency matrix of the network.',
                    default='results/net.mtx')
parser.add_argument('-nodedest',
                    help='output path to save labels of the nodes.',
                    default='results/nodes.txt')

args = parser.parse_args()

''' setting variables '''
tf_list_fn = args.tfdata
target_list_fn = args.targetdata
sig_tft_fn = args.sigtft
net_dest_fn = args.netdest
node_dest_fn = args.nodedest

def read_inputs(fn):
    # read tf list
    tf_list_data = pd.read_table(tf_list_fn, sep='\t', header=None, index_col=None)
    tfs = tf_list_data[0].values.tolist()

    # read target list
    target_annot_data = pd.read_table(target_list_fn, sep='\t', header=None, index_col=None)
    targets = target_annot_data[0].values.tolist()

    # create combined node list
    nodes = list(set(tfs+targets))
    n_nodes = len(nodes)

    # index nodes
    idx_dict = {nodes[i]:i for i in range(n_nodes)}

    # create adjacency matrix
    tft_data = pd.read_table(fn, sep='\t', header=0, index_col=None)
    tft_data = tft_data.drop_duplicates()
    rows = []
    cols = []
    for idx, row in tft_data.iterrows():
        try:
            rows.append(idx_dict[row['tf']])
            cols.append(idx_dict[row['gene']])
            rows.append(idx_dict[row['gene']])
            cols.append(idx_dict[row['tf']])
        except:
            do_nothing = True # some tf or gene (for e.g. '-') is not present
            print('Not found: ' + str(row['tf']) + ', ' + str(row['gene']))
    data = np.ones(len(rows), dtype=bool)
    A = sp.coo_matrix((data,(rows,cols)), shape=(n_nodes, n_nodes), dtype=bool)
    return nodes, A


nodes, A = read_inputs(sig_tft_fn)

# save nodes and A
with open(node_dest_fn, 'w') as fh:
    text = '\n'.join(nodes)
    fh.write(text)
sio.mmwrite(net_dest_fn, A, field='integer')
print('Done! Please find results in 1) ' + args.netdest + ' and 2) ' + args.netdest)

