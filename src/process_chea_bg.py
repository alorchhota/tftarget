'''
This script processes the tf-target file downloaded from http://amp.pharm.mssm.edu/lib/cheadownload.jsp.
The give contains both human and mouse tf-target. We would use only human data.
We would also keep only two columns: tf and targets.
Moreover, tf-target pairs would be unique.
'''

import os
import pandas as pd
import argparse

''' argument parsing '''
print(os.path.basename(__file__) +  ': parsing arguments ...')
parser = argparse.ArgumentParser()
parser.add_argument('-home',
                    help='home directory',
                    default='.')
parser.add_argument('-cheadata',
                    help='path to chea data file.',
                    default='data/chea-background.csv')
parser.add_argument('-tftargetdest',
                    help='output path to save tf-target data.',
                    default='results/tf-target-chea.txt')

args = parser.parse_args()

''' setting variables '''
home_dir = args.home
chea_data_path = args.cheadata
tf_target_dest = args.tftargetdest

# set working directory
os.chdir(home_dir)

print('reading chea data ...')
chea_data = pd.read_table(chea_data_path, sep=',', header=None, index_col=None, dtype=str,
                          names=['id','tf', 'tf_pubmed', 'target', 'tf_target_pubmed', 'exp_type', 'cell', 'organism', 'date'])

print('processing data ...')
org = [o.lower() for o in chea_data['organism']]
filtered_idx = [i for i in range(chea_data.shape[0]) if org[i]=='human']
filtered_data = zip(chea_data['tf'][filtered_idx], chea_data['target'][filtered_idx])
filtered_data = set(filtered_data)

print('saving processed tf-target data ...')
with open(tf_target_dest, 'w') as fh:
    fh.write('\t'.join(['tf','target']) + '\n')
    text = '\n'.join([ '\t'.join([str(item) for item in list(tup)]) for tup in filtered_data])
    fh.write(text)

print('Done! See results in ' + tf_target_dest)