'''
This script contains the necessary functions to calculate tf-target enrichment in a gold set.
'''

import pandas as pd
import numpy as np
import random
import scipy.stats.distributions as dist
import multiprocessing
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.sparse as sp


tft_pairs = set()
def load_gold_tft_from_file(fn, tfcol='tf', targetcol='target', **kwargs):
    '''
    loads (tf, target) pairs from a delimited file (formatted like a table).
    :param fn: file name.
    :param tfcol: column title or column index for tf.
    :param targetcol: column title or column index for target.
    :param kwargs: any argument to pass to the pandas.io.parsers.read_table() function to read the file correctly.
    :return: none
    '''
    if type(tfcol) is not int and type(tfcol) is not str:
        raise 'tfcol must be either an integer or a string.'
    if type(targetcol) is not int and type(targetcol) is not str:
        raise 'targetcol must be either an integer or a string.'

    data = pd.read_table(fn, **kwargs)
    global tft_pairs
    tft_pairs = set(zip(data[tfcol], data[targetcol]))

def load_gold_tft_from_iterator(iterator):
    '''
    loads (tf, target) pairs from an iterator.
    :param iterator: an iterator of (tf,target) tuple
    :return: none
    '''
    global tft_pairs
    tft_pairs = set(list(iterator))

def get_adj_mat_for_known_ttf(nodes, directed=False):
    ''' creates a adjacency matrix as if the known tf-targets together build a network.
    :param nodes: list of all node labels.
    :param directed: boolean. is it a directed graph?
    :return: adjacency matrix.
    '''
    global tft_pairs
    if len(tft_pairs) == 0:
        raise "gold tf-targets must be loaded first."

    n_nodes = len(nodes)
    tfs = [pair[0] for pair in tft_pairs]
    targets = [pair[1] for pair in tft_pairs]
    tft_nodes = set(tfs + targets)
    tft_nodes_idx = {}
    for node in tft_nodes:
        try:
            tft_nodes_idx[node] = nodes.index(node)
        except:
            tft_nodes_idx[node] = -1
    rows = []
    cols = []
    for tf, target in tft_pairs:
        r = tft_nodes_idx[tf]
        c = tft_nodes_idx[target]
        if r < 0 or c < 0:
            continue
        rows.append(r)
        cols.append(c)
        if directed==False:
            rows.append(c)
            cols.append(r)
    data = np.ones(len(rows), dtype=bool)
    adj = sp.coo_matrix((data, (rows, cols)), shape=(n_nodes, n_nodes), dtype=bool)
    return adj

def get_random_network_v0(n_nodes, n_edges):
    '''
    creates a random network with given number of nodes and edges.
    :param n_nodes: int. number of nodes.
    :param n_edges: int. number of edges.
    :return: adjacency matrix.
    '''
    net = np.zeros((n_nodes, n_nodes), dtype=bool)
    e_added = 0
    while e_added < n_edges:
        n1 = random.randint(0, n_nodes-1)
        n2 = random.randint(0, n_nodes-1)
        if n1 != n2 and net[n1,n2]!=True:
            net[n1,n2] = True
            e_added += 1
    return np.matrix(net)

def get_random_network_v0_5(A, nodes, tfs=None, targets=None, num=1):
    '''
    creates random networks.
    Every network must have same structure as the given network (A).
    Every source and target must be an element of tfs and targets, respectively.
    :param A: matrix. The adjacency matrix of the given network.
    :param nodes: list. All node labels in the same order used in the adjacency matrix.
    :param tfs: set. set of TFs. If None, 'nodes' is used instead.
    :param targets: set. set of targets. if None, 'nodes' is used instead.
    :param num: int. number of networks to generate.
    :return:
    '''
    if len(nodes) <= 0:
        raise 'There must have at least one node.'
    if tfs is None: tfs = set(nodes)
    if targets is None: targets = set(nodes)

    n_nodes = len(nodes)

    # create inverted index of each node, as we need row/col index of a node in A.
    nodes_inv_idx = {nodes[i]:i for i in range(len(nodes))}

    # find 3 groups of nodes, only tfs, only targets, both
    both_tft = tfs.intersection(targets)
    only_tfs = tfs.difference(both_tft)
    only_targets = targets.difference(both_tft)

    try:
        both_tft_idx = [nodes_inv_idx[v] for v in both_tft]
        only_tfs_idx = [nodes_inv_idx[v] for v in only_tfs]
        only_targets_idx = [nodes_inv_idx[v] for v in only_targets]
    except:
        raise 'every tf & target must be a subset of nodes.'

    for _ in range(num):
        # permutate within each group
        both_tft_idx_new = np.random.permutation(both_tft_idx)
        only_tfs_idx_new = np.random.permutation(only_tfs_idx)
        only_targets_idx_new = np.random.permutation(only_targets_idx)

        # create a combined new node map
        node_map = range(n_nodes)
        for i in range(len(both_tft_idx)):
            node_map[both_tft_idx[i]] = both_tft_idx_new[i]
        for i in range(len(only_tfs_idx)):
            node_map[only_tfs_idx[i]] = only_tfs_idx_new[i]
        for i in range(len(only_targets_idx)):
            node_map[only_targets_idx[i]] = only_targets_idx_new[i]

        # create new adjacency matrix with renamed nodes
        #A = sp.coo_matrix(([1,1,1,1],([0,1,1,4],[0,2,2,3])), shape=(5,5), dtype=bool)
        new_row = [node_map[r] for r in A.row]
        new_col = [node_map[c] for c in A.col]
        new_data = [d for d in A.data]
        new_A = sp.coo_matrix((new_data, (new_row, new_col)), dtype=bool, shape=A.shape)
        yield new_A

def get_random_network(A, only_tfs_idx, only_targets_idx, both_tft_idx):
    '''
    creates a random network permuting node labels.
    Every network must have same structure as the given network (A).
    Every source and target must be an element of tfs and targets, respectively.
    :param A: matrix. The adjacency matrix of the given network.
    :return: matrix. The adjacency matrix of random network.
    '''
    # permutate within each group
    both_tft_idx_new = np.random.permutation(both_tft_idx)
    only_tfs_idx_new = np.random.permutation(only_tfs_idx)
    only_targets_idx_new = np.random.permutation(only_targets_idx)

    # create a combined new node map
    node_map = range(A.shape[0])
    for i in range(len(both_tft_idx)):
        node_map[both_tft_idx[i]] = both_tft_idx_new[i]
    for i in range(len(only_tfs_idx)):
        node_map[only_tfs_idx[i]] = only_tfs_idx_new[i]
    for i in range(len(only_targets_idx)):
        node_map[only_targets_idx[i]] = only_targets_idx_new[i]

    # create new adjacency matrix with renamed nodes
    new_row = [node_map[r] for r in A.row]
    new_col = [node_map[c] for c in A.col]
    new_data = [d for d in A.data]
    new_A = sp.coo_matrix((new_data, (new_row, new_col)), dtype=bool, shape=A.shape)
    return new_A


def count_gold_tf_target(A, Agold, directed, d=1):
    '''
    counts number of gold tf-target presents in the given network.
    :param A: adjacency matrix of the network.
    :param Agold: Adjacency matrix of the gold network.
    :param d: int. max distance between two tf-target nodes in the given network (A).
    :param directed: bool. is directed graph?
    :return: int. number of tf-target of A present in Agold.
    '''
    Anet = A
    if d >= 2:
        Apow = A * A
        Anet = A + Apow
        for i in range(3,d+1):
            Apow = Apow * A
            Anet = Anet + Apow
    Anet = Anet.multiply(Agold)
    n_shared_edges = edge_count(Anet, directed)
    return n_shared_edges

def get_gold_tf_target(A, Agold, directed, nodes, d=1):
    '''
    get a list of gold tf-target presents in the given network.
    :param A: adjacency matrix of the network.
    :param Agold: Adjacency matrix of the gold network.
    :param d: int. max distance between two tf-target nodes in the given network (A).
    :param directed: bool. is directed graph?
    :return: list. list of tf-target pairs common in both A and Agold.
    '''
    Anet = A
    if d >= 2:
        Apow = A * A
        Anet = A + Apow
        for i in range(3,d+1):
            Apow = Apow * A
            Anet = Anet + Apow
    Anet = Anet.multiply(Agold)
    if directed==False:
        Anet = sp.triu(Anet)
    shared_idx = Anet.nonzero()
    unique_shared_idx = set(zip(shared_idx[0], shared_idx[1]))
    shared_edges = [(nodes[e[0]], nodes[e[1]]) for e in unique_shared_idx if nodes[e[0]] != nodes[e[1]]]
    return shared_edges


def single_round_enrichment(A, only_tfs_idx, only_targets_idx, both_tft_idx, Agold, d, directed):
    '''
    This method is used to parallelize calculation of fraction of gold edges present in random networks.
    :param A: matrix. adjacency matrix of given network.
    :param only_tfs_idx: list. indexes of nodes that can be used as tf, but not as target.
    :param only_targets_idx: list. indexes of nodes that can be used as target, but not as tf.
    :param both_tft_idx: list. indexes of nodes that can be used as both tf and target.
    :param Agold: matrix. adjacency matrix of gold network.
    :param d: int. max distance between two tf-target nodes in the given network (A).
    :param directed: bool. is directed graph?
    :return: float. fraction of gold edges present in a random network.
    '''
    Arand = get_random_network(A, only_tfs_idx, only_targets_idx, both_tft_idx)
    n_edge = edge_count(A, directed)
    frac = count_gold_tf_target(Arand, Agold, directed, d) / (n_edge+0.0)
    return frac

def edge_count(A, directed):
    '''
    counts the number of edges in a network.
    :param A: matrix. adjacency matrix of given network.
    :param directed: bool. is directed graph?
    :return: int. number of edges.
    '''
    nonz = A.nonzero()
    if directed==True:
        #ne = len(set(zip(nonz[0], nonz[1])))
        uniq_edges = set(zip(nonz[0], nonz[1]))
        # do not count self-to-self edges
        ne = sum([1 for e in uniq_edges if e[0] != e[1]])
    else:
        uniq_edges = set(zip(nonz[0], nonz[1]))
        # count only upper triangle wheren row_idx < col_idx (do not count self-to-self edges)
        ne = sum([1 for e in uniq_edges if e[0] < e[1]])
    return ne

def enrichment(A, nodes,
               tfs=None,
               targets=None,
               d=1,
               directed=False,
               n_trial=10000,
               n_proc=1,
               bg_plot_dest=None,
               bg_data_dest=None,
               model_dist='none'):
    ''' calculates enrichment of a learned tf-target network.
    :param A: nxn matrix, where (i,j)-th entry denotes if there is a path from i-th node to j-th node.
    :param nodes: list of node labels (used tf and gene names). i-th entry is the label of i-th node.
    :param d: int. distance of between
    :param directed: boolean. is it a directed graph?
    :param n_trial: int. number of trails to construct background distribution.
    :param n_proc: int. number of processes to use for multiprocessing.
    :param bg_plot_dest: str. path to save background distribution plot.
    :param bg_data_dest: str. path to save background distribution data.
    :param model_dist: str. 'none' or 'norm' or 'beta' or 'weibull'. distribution to fit number/fraction of connections in the gold dataset.
    :return: (list, float, float). list of gold edges in A, fitted p-value, and empirical p-value of enrichment.
    '''
    if not sp.isspmatrix_coo(A):
        raise 'A must be a sparse coo_matrix'
    if A.shape[0] != A.shape[1]:
        raise "A square matrix expected."
    if len(nodes) != A.shape[0]:
        raise "Lenght of nodes must be equal to the number of rows in A."
    if A.shape[0] <= 1:
        raise "Multiple nodes expected in the network."
    if tfs is None: tfs = nodes
    if targets is None: targets = nodes
    tfs = set(tfs)
    targets = set(targets)

    # create inverted index of each node, as we need row/col index of a node in A.
    nodes_inv_idx = {nodes[i]:i for i in range(len(nodes))}

    # find 3 groups of nodes, only tfs, only targets, both
    both_tft = tfs.intersection(targets)
    only_tfs = tfs.difference(both_tft)
    only_targets = targets.difference(both_tft)

    try:
        both_tft_idx = [nodes_inv_idx[v] for v in both_tft]
        only_tfs_idx = [nodes_inv_idx[v] for v in only_tfs]
        only_targets_idx = [nodes_inv_idx[v] for v in only_targets]
    except:
        raise 'every tf & target must be a subset of nodes.'

    n_edges = edge_count(A, directed)
    if n_edges <= 0:
        raise "at least one edge must be present."

    Agold = get_adj_mat_for_known_ttf(nodes, directed=directed)
    proc = multiprocessing.Pool(n_proc)
    async_res = [proc.apply_async(single_round_enrichment, args=(A, only_tfs_idx, only_targets_idx, both_tft_idx, Agold, d, directed))
                 for _ in range(n_trial)]
    bg_data = [res.get() for res in async_res]
    log_bg_data = [np.log2(1+item) for item in bg_data] 
    #neg_log_bg_data = [-np.log10(item) for item in bg_data] # convert to positive number, needed for distribution fit

    n_gold_tft = count_gold_tf_target(A, Agold, directed, d)
    frac_gold_tft = n_gold_tft / (n_edges+0.0)
    # show gold tft fraction
    print('#n_gold_tft: ' + str(n_gold_tft))
    print('#edges: ' + str(n_edges))
    print('#fraction_gold_tft: ' + str(frac_gold_tft))
    # calculate p-value
    right_tail = [x >= frac_gold_tft for x in bg_data]
    empirical_pval = sum(right_tail) / (len(bg_data) + 0.0)
    shift = 0 # data needs to be shifted for weibull distribution
    if model_dist == 'beta':
        a,b,_,_ = dist.beta.fit(log_bg_data) # log_bg_data is in [0,1], also beta dist is defined over (0,1)
        bg_dist = dist.beta(a,b)
        pval = 1 - bg_dist.cdf(np.log2(1+frac_gold_tft))
    elif model_dist == 'norm':
        mu,std = dist.norm.fit(log_bg_data)
        bg_dist = dist.norm(mu,std)
        pval = 1 - bg_dist.cdf(np.log2(1+frac_gold_tft))
    elif model_dist == 'none':
        pval = empirical_pval
    elif model_dist == 'weibull':
        a, c, loc, scale  = dist.exponweib.fit(log_bg_data)
        bg_dist = dist.exponweib(a, c, loc=loc, scale=scale)
        pval = 1 - bg_dist.cdf(np.log2(1+frac_gold_tft))
    else:
        raise 'invalid value of the parameter: model_dist.'

    # save background distribution plot
    if bg_plot_dest is not None:
        h = plt.hist(log_bg_data, normed=True)
        plt.ylim((0, max(h[0])))
        if model_dist != 'none':
            min_x = min(np.append(h[1], np.log2(1+frac_gold_tft)))
            max_x = max(np.append(h[1], np.log2(1+frac_gold_tft)))
            x_vals = np.linspace(min_x, max_x, num=1000)
            y_vals = [bg_dist.pdf(x) for x in x_vals]
            plt.plot(x_vals, y_vals)
            plt.legend([' '.join([model_dist, 'fit']), 'randomly generated'])
        plt.axvline(np.log2(1+frac_gold_tft), color='r')
        plt.xlabel('log2(1+fraction of edges found in gold set)')
        plt.ylabel('pdf')
        plt.title('Background distribution')
        plt.show()
        plt.savefig(bg_plot_dest)
        plt.close()

    # save background distribution data
    if bg_data_dest is not None:
        with open(bg_data_dest, 'w') as fh:
            text = '\n'.join([str(v) for v in bg_data])
            fh.write(text)

    gold_tft_found = get_gold_tf_target(A, Agold, directed, nodes, d)
    return (gold_tft_found, pval, empirical_pval)

